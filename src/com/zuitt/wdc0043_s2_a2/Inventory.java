package com.zuitt.wdc0043_s2_a2;

import java.util.HashMap;

public class Inventory {
    public static void main(String[] args) {
        HashMap<String, Integer> inventory = new HashMap<>() {
            {
                this.put("toothpaste", 15);
                this.put("toothbrush", 20);
            }
        };
        inventory.put("soap", 12);
        System.out.println("Our current inventory consists of: " + inventory);
    }

}
