package com.zuitt.wdc0043_s2_a2;

public class PrimeNumber {

    public static void main(String[] args) {
        int[] intArray = new int[]{2, 3, 5, 7, 11};
        System.out.println("The first prime number is: "+ intArray[0]);
        System.out.println("The second prime number is: "+ intArray[1]);
        System.out.println("The third prime number is: "+ intArray[2]);
        System.out.println("The fourth prime number is: "+ intArray[3]);
        System.out.println("The fifth prime number is: "+ intArray[4]);


    }


}
