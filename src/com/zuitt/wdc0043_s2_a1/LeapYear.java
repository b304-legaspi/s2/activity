package com.zuitt.wdc0043_s2_a1;
import java.util.Scanner;

public class LeapYear {
    public static void main(String[] args) {


        Scanner scan = new Scanner(System.in);
        System.out.println("Input year to be checked if a leap year");
        int number = scan.nextInt();
        System.out.println(number);

        if (number % 100 == 0 && number % 400 != 0) {
            System.out.println(number + " is not a leap year.");
        } else if (number % 100 == 0 && number % 400 == 0) {
            System.out.println(number + " is a leap year.");
        } else if (number % 4 == 0) {
            System.out.println(number + " is a leap year.");
        } else {
            System.out.println(number + " is not a leap year.");
        }
    }
}
